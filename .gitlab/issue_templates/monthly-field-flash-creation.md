Want to contribute something to the newsletter? Have suggestions for process or general improvements? Capture them using [this quick feedback process](https://about.gitlab.com/handbook/sales/field-communications/#sharing-feedback), or reach out to @monicaj and @shannonthompson.

## [MONTH] Field Flash Newsletter

### Outline

1. Featured 
   - TBD
1. Deal of the month
   - TBD
1. New and noteworthy resources
   - TBD 
   - [Items from SM issue]
   - [Items from competitive intelligence issue]
1. Did You Know?  
   - TBD
1. What's new in GitLab
   - TBD
1. Upcoming events 
   - AMER West:
   - AMER East:
   - AMER PubSec: 
   - EMEA: 
   - APAC: 
1. Enablement corner 
   - TBD
1. Channel Center
   - TBD

### Draft
Once/as outline is complete, you can find the draft in [this Google doc.] ADD G-DOC LINK.

----

### Background
Field Flash is a monthly recurring newsletter that is sent to the field organization – sales, CS, and stakeholders. It recaps important general updates relevant to this audience in a short, digestable format. The newsletter is sent via email using Bananatag. For more information, see Handbook: https://about.gitlab.com/handbook/sales/field-communications/field-flash-newsletter/.

Questions about the newsletter or field communications?  Reach out to @monicaj and @shannonthompson.

### Project Checklist (Author)
* [ ]  Create issue at least two weeks before last Monday of the current month 
* [ ]  Set the due date as five business days following the newsletter launch date 
* [ ]  Create initial outline
* [ ]  Assign tasks to relevant content owners for each section where stakeholder input is needed
* [ ]  Once outline has been populated, draft content in this issue (Change issue label to ~"Field Newsletter::Draft")
* [ ]  Input content into newsletter draft in Bananatag
* [ ]  Send draft for review to relevant leaders and stakeholders no more than two business days before planned send date (Change issue label to ~"Field Newsletter::Review")
* [ ]  Schedule newsletter to send (Change issue label to ~"Field Newsletter::Scheduled")
* [ ]  Once newsletter goes live, send a reminder to field in the #sales and #customer-success Slack channels (Change issue label to ~"Field Newsletter::Sent")
* [ ]  Five business days following the newsletter send, capture newsletter performance data in the designated section below 

### DRIs/Contributors - CONTENT DUE DATE XXXX-XX-XX
Please edit/contribute to/review the outline above with any relevant updates related to the areas that you are tagged in. Provide as much detail as possible – including links to supporting resources.
1. Featured 
   * [ ]  NAME
1. Deal of the month
   * [ ]  NAME
1. New and noteworthy resources 
   * [ ]  NAME
1. Did You Know?
   * [ ]  NAME
1. What's new in GitLab
   * [ ]  NAME
1. Upcoming events 
   * [ ]  NAME
1. Enablement corner 
   * [ ]  NAME

### Reviewers
The newsletter test will be sent to you via email from Bananatag no later than two business days before the target send date. Please record any feedback via comments in this issue and include screenshots when possible. Reviewers should **provide feedback no later than 3 pm CT on the business day before the planned send date** to allow time for revisions and scheduling. Once you are done reviewing, please check off the task below assigned to you.
 
* [ ]  David Somers
* [ ]  James Harrison

### Important Notes 
* The newsletter is sent on the first Monday of every month at 9 am PT/11 am CT. (Avoiding holidays) Included content will focus on updates from the month prior.
* When possible, the newsletter will be timed to align with a bi-weekly WW Field Sales Call to increase visibility. 
* The newsletter is always named in accordance with the month it is *sent in*. i.e. The September newsletter is sent on the first Monday in September and primarily contains information/updates from the prior month of August.
* TBD - running list. We will add more as we have more learnings.

---- 
### Performance

Open rate - overall: 
Open rate - Sales division: 

CTR: 

Top 5 links clicked:

1. 
1. 
1. 
1. 
1. 

Lowest 5 performing links:

1. 
1. 
1. 
1. 
1. 

Notes:

- 

/label ~"field communications" ~"Field Newsletter::Outline" ~"field enablement" ~"FE priority::1" ~"FE status::wip"
/assign @monicaj @shannonthompson
