This issue template is for the ENT Enablement program manager and designed to capture a new training and enablement initiative for Enterprise Enablement. Title this issue with `Enablement Request: [Topic]`.

## Background 
<!-- Give background on the enablement request. Including: what is it, why does it matter (what problem is it trying to solve), why now, and what's the impact on the business -->

[Answer here]

**What problem is this trying to solve?**

[Answer here]

**What is the impact on the business?**

[Answer here]

**Who are the key stakeholders and geos:**

[Answer here]

**What is the urgency level of this request?**
- [ ] - Urgent, immediate action needed
- [ ] - Medium urgency, action preferred this quarter
- [ ] - Not urgent, no specific timeline

----
*Do not edit below
/label ~"field enablement" ~"FE status::wip" ~"FE: ENT Sales"
/assign @emelier

