## Course Summary
*Please provide a brief summary of the course.*

 * 

## Learning Objectives 
*Please outline at least 3 learning objectives to be accomplished as a result of the course.*

1. 
2. 
3. 

## SMEs 
*Which subject matter experts will you work with to create and or peer review course content?*
 
  * 

## Success Criteria
*What metrics can we measure to get a baseline and use to validate the efficacy of the course?*

 * 


## Roadmap 
* [ ] Identify 2-3 Champion SMEs
* [ ] Develop Learning Objectives 
* [ ] Share Learning Objectives with SMEs
* [ ] Validate Learning Objectives with Leadership 
* [ ] Create course outline
* [ ] Verify / create Handbook content 
* [ ] _For Field Cert Courses_ Build Manager Coaching Content 
* [ ] Develop e-learning
* [ ] Feedback with SMEs
* [ ] Release Alpha
* [ ] Incorporate Alpha Feedback
* [ ] Pilot course
* [ ] Launch course

## Content Authoring Tool
*What content authoring tool will be used to create this course?*
* [ ] Articulate Rise
* [ ] Articulate Storyline
* [ ] Other

## Course Settings

**1. Should the learner be allowed to navigate the course freely or one section at a time?**
* [ ] Free
* [ ] Restricted

**2. Does the course contain a quiz?**
* [ ] Yes
* [ ] No

**3. Must the learner pass a mandatory quiz before completing the course?**
* [ ] Yes
* [ ] No
* [ ] N/A 

**4. If the course contains a mandatory quiz, what is the minimum passing score?**
* [ ] 80%
* [ ] 100%
* [ ] N/A 

**5. Should reporting for course completion be based on passing a quiz?**
   * *Note: If course completion is not based on passing a quiz, the learner will be able to click through the course and register as complete after viewing all content.*
* [ ] Yes
* [ ] No

**6. Was the course created internally or by a consultant or outside party?**
   * *Note: If the course was created internally, please add John Blevins as a "Course Manager" to verify settings and download SCORM files.*
* [ ] Internally
* [ ] Externally 

**7. If the course was created by a consultant or outside party, have you verified the settings for the above questions with the course creator?** 
   * Note: *Please request for the SCORM course to be downloaded in the SCORM 2004 v4 format. Additionally, please request that course includes an exit link and that reporting be set to show "Complete/Incomplete"*
* [ ] Yes
* [ ] No
* [ ] N/A 

**8. If the course was created externally, please paste a link to the SCORM file in google drive.**

* 

**9. Does this course contain an embedded survey?**
* [ ] Yes
* [ ] No

**10. If the course contains an embedded survey, please paste the survey link below.** 

* 


## EdCast Course Upload & Settings

**1. In what format should the course be uploaded into EdCast?**
* [ ] Single Smart Card
* [ ] Pathway with multiple Smart Cards
* [ ] Journey with multiple Pathways & Smart Cards

**2. Should this course be restricted to GitLab internal audiences only?** 
  * *Note: If you answer no, the course will be visible to partners and customers.*
* [ ] Yes
* [ ] No

**3. Should this content be shared with a particular group or assigned with a due date for completion?**
* [ ] Shared
* [ ] Assigned with a due date
* [ ] Both 

**4. Which general or role based content channel(s) should feature this course? (please select all that apply)**
* [ ] Sales Quick Start
* [ ] Field Enablement Channel 
* [ ] SDR - Sales Development 
* [ ] AE - Account Executives 
* [ ] SAL - Strategic Account Leaders 
* [ ] TAM - Technical Account Managers
* [ ] SA - Solutions Architects
* [ ] PSE - Professional Services Engineers
* [ ] CAM - Channel Account Managers
* [ ] Field Sales Leaders & Managers 
* [ ] Channel (external partners)

**5. Which group(s) should be assigned this content? (please select all that apply)**
* [ ] SDR - Sales Development 
* [ ] AE - Account Executives 
* [ ] SAL - Strategic Account Leaders 
* [ ] TAM - Technical Account Managers
* [ ] SA - Solutions Architects
* [ ] PSE - Professional Services Engineers
* [ ] CAM - Channel Account Managers
* [ ] Field Sales Leaders & Managers 
* [ ] Channel (external partners)

**6. Does this course contain an application exercise?**
* [ ] Yes 
* [ ] No  

**7. If this course contains an application exercise, what is the preferred grading scale?**
* [ ] 1 - 10 points  
* [ ] 0 - 100 points
* [ ] A - F   
* [ ] N/A 

**8. What tags should be added to the course smart card for ease of searching and discoverability?**

1. 
2. 
3. 

**9. Please provide a Handbook link that references this course's availability in EdCast.**

* 

**10. What should the course be named in EdCast?** 

* 

**11. What date should the content be assigned or shared?** 

* 

**12. What date should the assignment be due?**

* 

**13. What date should the first progress report be made avaliable?**

* 

**14. How often should progress reports be made avaliable for this course?**
* [ ] Daily
* [ ] Twice Weekly 
* [ ] Weekly
* [ ] Every Other Week
* [ ] Monthly 

**15. What day of the week would you like progress reports to be made avaliable?**

* [ ] Monday
* [ ] Tuesday
* [ ] Wednesday
* [ ] Thursday
* [ ] Friday

**16. To whom should progress reports be sent?**

* 

**17. Progress Report Link**

*

**18. Does this course replace an existing course?** 

   * Note: *This is important for making sure handbook pages that reference courses are updated with new links when new versions are released.* 
* [ ] Yes 
* [ ] No 

**19. If this course replaces an existing course, please copy the link to the old course** 

* 

**20. Should this course be configured to award a badge for successful completion?** 
* [ ] Yes 
* [ ] No 

**21. If this course should award a badge for completion please submit a link to the badge and write the badge name.**

* Badge Link:

* Badge Name:


/label ~"FE status::triage" ~"field enablement"
/assign @jblevins608
