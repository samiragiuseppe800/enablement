For Event Planners

### Event Checklist
* [ ] Contract Signed
* [ ] Event added to Events Cal and Events page
* [ ] Invoice paid
* [ ] Artwork sent
* [ ] Booth Artwork sent 
* [ ] Slack channel created and attendees invited
* [ ] Planning spreadsheet (included travel, booth duty, meetings...)
* [ ] Tickets allocated   
* [ ] Attendee directory from organizers
* [ ] AV ordered
* [ ] Furnishings ordered
* [ ] Electrical if needed
* [ ] Flights/ transport booked- added to spreadsheet
* [ ] Final prep meeting scheduled
* [ ] Event post mortem scheduled 
