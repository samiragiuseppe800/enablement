### Sales & Customer Success Quick Start Modification Request

This issue template is designed to capture your feedback about improvements and iteration to Sales Quick Start. (GitLab's functional sales onboarding training) If you need to request multiple changes; please create an issue with this template for each unrelated modification request.

### Type of Modification
Please select all that apply to the requested modification. 

- [ ]  Adding New Information
- [ ]  Changing Assignments 
- [ ]  Deleting Outdated Information
- [ ]  Changing Order of Information or Assignments 

### Which Section of Learning Path Should be Modified?
- [ ]  Welcome to GitLab Sales 
- [ ]  DevOps Technology Landscape
- [ ]  Our Customers 
- [ ]  Our Portfolio
- [ ]  Field Roles & Expectations 
- [ ]  Sales Process
- [ ]  Sales Action & Roleplays
- [ ]  Our Competition 
- [ ]  Tools to Get Your Job Done faster
- [ ]  Sales & Customer Success Support 
- [ ]  GitLab Customer Support Team
- [ ]  Technical Deep Dive (SA, TAM, and PSE ONLY)
- [ ]  Integrations (SA, TAM, and PSE Only)
- [ ]  SAs Only: Set Up Your Demo Environment
- [ ]  TAMs Only: Customer Onboarding
- [ ]  PSEs Only: Statement of Work
- [ ]  In-Class Assignments
- [ ]  Post-Class Assignments
- [ ]  Course Evaluation & Feedback


### Describe your Suggestion: 
Please be specific

- Describe your change here:



- Describe why this will help:



### Request Completion Checklist
- [ ]  Update Handbook 
- [ ]  Update Relevant Rise Course
- [ ]  Update Current Sales Onboarding Pathway in EdCast
- [ ]  Inform Channel Enablement for Review




/label ~"sales onboarding"
/label ~"FE priority::new request"
/label ~"FE status::triage"
/label ~"field enablement" 
/assign @jblevins608
