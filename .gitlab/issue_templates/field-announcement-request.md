This issue template is designed to capture your request for an announcement to the GitLab Field (Sales, CS, Channel, Alliances, Ops) team. Title this issue with `Field Announcement Request: [Topic]`.

## Background 
<!-- Provide details about the announcement that you are interested in making to the field organization, including context on urgency and impact to the field, and expected outcomes/calls-to-action. Include as much detail as possible. -->
[Answer here]

### Request Info
<!--Your target announcement date should be at least two business days from when you submit this completed request. If your target announcement date is less than two business days from the time you submit your completed request due to a high impact/high urgency announcement (system outage, immediate customer impact, etc.) that needs to be shared with the Field, please submit your request as soon as possible and send a Slack message to a member of the Field Communications team to prioritize.-->

+ Target announcement date: 
+ DRI(s): 
+ Recommended spokesperson: 
+ Related issue(s) and/or Handbook page(s): 

### Announcement Scope 
<!-- Choose an option in each of the categories below. Delete all unselected options. This information is mandatory and will be used to generate the [post header](https://about.gitlab.com/handbook/sales/sales-google-groups/field-fyi-channel/##field-fyi-post-headers) for the #field-fyi Slack channel (if applicable). -->

1. **Audience**
   - [ ] All Field (Sales, CS, Channel & Alliances)
   - [ ] All Sales (ENT & COMM)
   - [ ] ENT Sales
   - [ ] COMM Sales
   - [ ] Customer Success
   - [ ] Channel Team
   - [ ] Alliances Team
   - [ ] Field Operations
   - [ ] GitLab Partners (external)
1. **Topic**
   - [ ] Operations
   - [ ] Enablement
   - [ ] Competitive Intelligence
   - [ ] Customer Reference
   - [ ] Product Marketing
   - [ ] Channel/Alliances Update
   - [ ] Marketing
   - [ ] Product
   - [ ] Pricing & Packaging 
   - [ ] Other [please type in answer]
1. **Type**
   - [ ] New mandatory enablement
   - [ ] New optional enablement
   - [ ] System Changes/System Updates
   - [ ] Bookings Update
   - [ ] New Resource
   - [ ] Survey
   - [ ] Process Change
   - [ ] Roadmap Update
   - [ ] Org Announcement
   - [ ] Other [please type in answer]
1. **Urgency**
   - [ ] 🚨 Action Required - urgent, action required with a deadline
   - [ ] 🧠 Need to Know - urgent, update directly impacts audiences' workflows/processes
   - [ ] 📊 Feedback Requested - less urgent, action requested (with or without long-lead deadline) but not required
   - [ ] 👀 Review - less urgent, does not directly or materially impact audiences' workflows/processes

### Draft Copy
<!-- If you have an idea of what you'd like the announcement to say (i.e. a draft Slack message) please input it here. Not required, but helpful for the Field Comms team if you already have something in mind. -->

[Answer here]

## Field Communications Playbook 
<!-- Select a recommended approach after consulting the Field Communications Playbook handbook page: https://about.gitlab.com/handbook/sales/field-communications/#field-communications-playbook. -->

**Update Tier**
- [ ]  Tier 1
- [ ]  Tier 2
- [ ]  Tier 3

**Communication Channels**
- [ ]  #sales general channel
- [ ]  #customer-success general channel
- [ ]  #channel-fyi channel 
- [ ]  [#field-fyi announcement channel](https://about.gitlab.com/handbook/sales/sales-google-groups/field-fyi-channel/)
- [ ]  #field-managers channel
- [ ]  [Sales Handbook](https://about.gitlab.com/handbook/sales/)
- [ ]  [Field Enablement Webinar](https://about.gitlab.com/handbook/sales/training/sales-enablement-sessions/)
- [ ]  [CS Skills Exchange Webinar](https://about.gitlab.com/handbook/sales/training/customer-success-skills-exchange/)
- [ ]  [Field Flash newsletter](https://about.gitlab.com/handbook/sales/field-communications/field-flash-newsletter/)
- [ ]  [WW Field Sales Call](/handbook/sales/#bi-weekly-monday-sales-call)
- [ ]  GTM Field Update

### Please confirm that you have done the following prior to submitting this issue request: 
- [ ]  Consulted the [Field Communications Playbook](https://about.gitlab.com/handbook/sales/field-communications/#field-communications-playbook)
- [ ]  Considered the qualifying questions in the Playbook and provided a recommendation on Tier and Communication Channels accordingly. 
- [ ]  Provided detailed background and reference materials about the requested announcement. 

After submitting this request you'll be able to track your request on the [Field Announcements Request Board.](https://gitlab.com/gitlab-com/sales-team/field-operations/enablement/-/boards/1746168?label_name[]=Field%20Announcement)

cc @Bfraser

----
*Do not edit below
/label ~"Field Announcement" ~"Field Announcement::Triage" ~"field communications" ~"field enablement" ~"FE priority::new request" ~"FE status::triage"
/assign @monicaj @shannonthompson
