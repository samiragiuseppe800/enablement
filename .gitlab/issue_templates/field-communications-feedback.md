## **Title: Summarize Your Suggestion**
This issue template is designed to capture your feedback and/or ideas for Field Communications.

### Type of feedback
<!-- Select the category/categories your feedback or ideas most directly relate to from the list below and delete the others -->

- [ ]  [Field Flash Newsletter](https://about.gitlab.com/handbook/sales/field-communications/field-flash-newsletter/)
- [ ]  GitLab Product and Feature Updates 
- [ ]  Field Enablement Resources Updates
- [ ]  Segment-Specific Communication Request (i.e. ENT, COMM, PubSec, CS)
- [ ]  Other (please describe)

### Details 
Describe your suggestion: (please be specific)



- describe why this will help:



- link to any supporting materials, if applicable:



---
*Do not edit below
/label ~"FC Feedback" ~"FC Feedback::New" ~"field communications" ~"field enablement"
/assign @monicaj
