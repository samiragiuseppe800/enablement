**First Day of Enrollment:**

* (Date TBD)

**Last Day of Enrollment:**

* (Date TBD)

**Welcome Call**: 

* (Date TBD)

**Start Date:**

* (Date TBD)

**EdCast Pre-Work Pathway:**

* Link: https://gitlab.edcast.com/pathways/sales-quick-start

**Class Roster, Mock Call Groups & Grade Report:** 

* (Link TBD)

**Pre-Class Survey**

* [Pre Workshop Survey Responses](https://docs.google.com/forms/d/1QEM28wCroVYLIN-UMJr0gXpFamOTnc4aZnFLM8d6MSA/edit#responses)

**Post-Class Survey**

* [Post Workshop Survey Responses](https://docs.google.com/forms/d/1MuXRKieYh86cffByFl6JCY2HiZ7A5vlWQb17tZalhqE/edit#responses)


## SQS Checklist:
* [ ] Confirm SQS start date
* [ ] Attendee spreadsheet created
* [ ] Create EdCast group for SQS cohort
* [ ] Create SQS cohort Slack channel
* [ ] Add attendees to SQS Slack channel
* [ ] Assign attendees to SQS pathway in EdCast
* [ ] Send Welcome Call calendar invite
* [ ] Engage PMMs and SMEs for SQS attendance/presentation 
* [ ] Create SQS live session notes document
* [ ] Send calendar invites for SMEs presenting an SQS session
* [ ] Send calendar invites for PMMs facilitating mock calls
* [ ] Assign [Introduction to the Required 7](https://gitlab.edcast.com/journey/introduction-to-the-required-introduction) journey to COM Sales team members
* [ ] Assign [Customer Success: Technical Deep Dive](https://gitlab.edcast.com/insights/customer-success-technical) course to Customer Success team members

## SQS Agenda:

| DATE | START TIME | END TIME | ACTIVITY | SME ASSIGNED |
| ------ | ------ | ------ | ------ | ------ |
| May 17, 2021 | 1:00p ET | 2:30p ET | Essential Questions Exercise | @jblevins608  |
| May 17, 2021 | 3:00p ET | 4:30p ET | Value Card Exercise | @jblevins608  |
| May 18, 2021 | 1:00p ET | 2:30p ET | Discovery Question Exercise | @jblevins608  |
| May 18, 2021 | 3:00p ET | 4:30p ET | Differentiator Exercise | @jblevins608  |
| May 19, 2021 | 1:00p ET | 2:30p ET | MEDDPPICC & Breakout Call Prep | @jblevins608  |
| May 19, 2021 | 3:00p ET | 4:00p ET | Discovery Call 1 | @jblevins608  + Mock Customers  |
| May 20, 2021 | 1:00p ET | 2:00p ET | Discovery Call 2 | @jblevins608  + Mock Customers |
| May 20, 2021 | 2:00p ET | 2:45p ET | No Tissues for Issues + Product Tiering |   |
| May 20, 2021 | 3:45p ET | 4:30p ET | Alliances |   |
| May 24, 2021 | 1:00p ET | 1:45p ET | Discussion: Professional Services |   |
| May 24, 2021 | 2:00p ET | 3:00p ET | Discovery Call 3 | @jblevins608  + Mock Customers |
| May 25, 2021 | 1:00p ET | 2:00p ET | Discovery Call 4 | @jblevins608  + Mock Customers |
| May 25, 2021 | 2:00p ET | 3:00p ET | Territory Planning | @jblevins608  |
| May 25, 2021 | 3:00p ET | 4:30p ET | Intro to Competition |   |
| May 26, 2021 | 12:15p ET | 1:00p ET | Legal / Deal Desk |  |
| May 26, 2021 | 1:00p ET | 1:45p ET | Field Security | @gitlab-com/gl-security/security-assurance/field-security-team    |
| May 26, 2021 | 2:00p ET | 2:45p ET | Channels |    |


cc: @jblevins608 @dcsomers @kreykrey @kshirazi @monicaj @pdaliparthi @tbopape @emelier

/label ~"sales onboarding" ~"field enablement"
/label ~"FE status::triage"
/assign @jblevins608
